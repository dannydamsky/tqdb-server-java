package com.damsky.danny.tqdb.data.dao;

import com.damsky.danny.tqdb.data.entity.Question;

import java.util.List;

/**
 * Interface for implementing DAO classes of type {@link Question}.
 *
 * @author Danny Damsky
 * @see Dao
 * @see Question
 */
public interface QuestionDao extends Dao<Question> {

    /**
     * Queries the database for rows that are confirmed by the admin of the webapp,
     * done in a single transaction.
     *
     * @return All rows that are confirmed by the admin of the webapp.
     */
    List<Question> getAllConfirmed();

    /**
     * @param count The amount of rows to get.
     * @return Random rows
     */
    List<Question> getRandom(int count);

    /**
     * @param count The amount of rows to get.
     * @return Random rows that are confirmed by the admin of the webapp.
     */
    List<Question> getRandomConfirmed(int count);

    /**
     * @param rowFrom The row to start querying from.
     * @param rows    The amount of rows to get.
     * @return All rows from rowFrom to rowFrom + rows;
     */
    List<Question> getAllFromTo(int rowFrom, int rows);

    /**
     * @param rowFrom The row to start querying from.
     * @param rows    The amount of rows to get.
     * @return All rows that are confirmed by the admin of the webapp
     * from rowFrom to rowFrom + rows;
     */
    List<Question> getAllFromToConfirmed(int rowFrom, int rows);
}
