 A website where you can play a game of trivia. If you register, your progress will be saved. You can also add your own questions and if they are confirmed by the admin then they might appear in your next games. The website is a Web Application with an API written using the following technologies:

    Java Jersey
    Apache Tomcat
    MySQL Server
    Hibernate ORM
    j2html HTML-Builder

Link to website: http://ec2-18-222-152-255.us-east-2.compute.amazonaws.com/tqdb