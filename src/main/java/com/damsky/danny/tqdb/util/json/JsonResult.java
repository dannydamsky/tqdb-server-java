package com.damsky.danny.tqdb.util.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class is meant to be returned from API calls.
 * It's purpose is to display a message to a user of the API containing
 * important data.
 *
 * @author Danny Damsky
 */
@XmlRootElement
public final class JsonResult {

    /**
     * @return Default success message instance of the class.
     */
    public static JsonResult success() {
        return new JsonResult("SUCCESS", null);
    }

    /**
     * @param explanation The explanation to display in the JSON.
     * @return Default error message instance of the class.
     */
    public static JsonResult error(String explanation) {
        return new JsonResult("ERROR", explanation);
    }

    @JsonProperty("message")
    private String message;
    @JsonProperty("explanation")
    private String explanation;

    // Reserved for Jackson JSON Parser
    public JsonResult() {
    }

    public JsonResult(final String message, final String explanation) {
        this.message = message;
        this.explanation = explanation;
    }

    @Override
    public String toString() {
        return "JsonResult{" +
                "message='" + message + '\'' +
                ", explanation='" + explanation + '\'' +
                '}';
    }
}
