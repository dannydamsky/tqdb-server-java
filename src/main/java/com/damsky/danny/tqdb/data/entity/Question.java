package com.damsky.danny.tqdb.data.entity;

import com.damsky.danny.tqdb.data.DbConstants;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A class representing the questions table in the database.
 *
 * @author Danny Damsky
 * @see User
 * @see Game
 */
@Entity
@Table(name = DbConstants.QUESTIONS_TABLE_NAME)
public final class Question implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbConstants.QUESTIONS_COLUMN_ID, nullable = false)
    private long id;
    @Column(name = DbConstants.QUESTIONS_COLUMN_QUESTION, nullable = false, unique = true)
    private String question;
    @Column(name = DbConstants.QUESTIONS_COLUMN_REAL_ANSWER, nullable = false)
    private String realAnswer;
    @Column(name = DbConstants.QUESTIONS_COLUMN_FIRST_FAKE_ANSWER, nullable = false)
    private String firstFakeAnswer;
    @Column(name = DbConstants.QUESTIONS_COLUMN_SECOND_FAKE_ANSWER, nullable = false)
    private String secondFakeAnswer;
    @Column(name = DbConstants.QUESTIONS_COLUMN_THIRD_FAKE_ANSWER, nullable = false)
    private String thirdFakeAnswer;
    @Column(name = DbConstants.QUESTIONS_COLUMN_CONFIRMED, nullable = false)
    private boolean confirmed;

    private Question(Builder builder) {
        this.id = builder.id;
        this.question = builder.question;
        this.realAnswer = builder.realAnswer;
        this.firstFakeAnswer = builder.firstFakeAnswer;
        this.secondFakeAnswer = builder.secondFakeAnswer;
        this.thirdFakeAnswer = builder.thirdFakeAnswer;
        this.confirmed = builder.confirmed;
    }

    public Question() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getRealAnswer() {
        return realAnswer;
    }

    public void setRealAnswer(String realAnswer) {
        this.realAnswer = realAnswer;
    }

    public String getFirstFakeAnswer() {
        return firstFakeAnswer;
    }

    public void setFirstFakeAnswer(String firstFakeAnswer) {
        this.firstFakeAnswer = firstFakeAnswer;
    }

    public String getSecondFakeAnswer() {
        return secondFakeAnswer;
    }

    public void setSecondFakeAnswer(String secondFakeAnswer) {
        this.secondFakeAnswer = secondFakeAnswer;
    }

    public String getThirdFakeAnswer() {
        return thirdFakeAnswer;
    }

    public void setThirdFakeAnswer(String thirdFakeAnswer) {
        this.thirdFakeAnswer = thirdFakeAnswer;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", realAnswer='" + realAnswer + '\'' +
                ", firstFakeAnswer='" + firstFakeAnswer + '\'' +
                ", secondFakeAnswer='" + secondFakeAnswer + '\'' +
                ", thirdFakeAnswer='" + thirdFakeAnswer + '\'' +
                ", confirmed=" + confirmed +
                '}';
    }

    public static final class Builder {
        private long id;
        private String question;
        private String realAnswer;
        private String firstFakeAnswer;
        private String secondFakeAnswer;
        private String thirdFakeAnswer;
        private boolean confirmed = false;

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setQuestion(String question) {
            this.question = question;
            return this;
        }

        public Builder setRealAnswer(String realAnswer) {
            this.realAnswer = realAnswer;
            return this;
        }

        public Builder setFirstFakeAnswer(String firstFakeAnswer) {
            this.firstFakeAnswer = firstFakeAnswer;
            return this;
        }

        public Builder setSecondFakeAnswer(String secondFakeAnswer) {
            this.secondFakeAnswer = secondFakeAnswer;
            return this;
        }

        public Builder setThirdFakeAnswer(String thirdFakeAnswer) {
            this.thirdFakeAnswer = thirdFakeAnswer;
            return this;
        }

        public Builder setConfirmed() {
            this.confirmed = true;
            return this;
        }

        public Question build() {
            return new Question(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "id=" + id +
                    ", question='" + question + '\'' +
                    ", realAnswer='" + realAnswer + '\'' +
                    ", firstFakeAnswer='" + firstFakeAnswer + '\'' +
                    ", secondFakeAnswer='" + secondFakeAnswer + '\'' +
                    ", thirdFakeAnswer='" + thirdFakeAnswer + '\'' +
                    ", confirmed=" + confirmed +
                    '}';
        }
    }
}