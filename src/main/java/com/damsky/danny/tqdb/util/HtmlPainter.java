package com.damsky.danny.tqdb.util;

import com.damsky.danny.tqdb.data.entity.Game;
import com.damsky.danny.tqdb.data.entity.Question;
import com.damsky.danny.tqdb.data.entity.User;
import j2html.tags.ContainerTag;
import j2html.tags.EmptyTag;

import java.util.List;
import java.util.Random;

import static j2html.TagCreator.*;
import static j2html.attributes.Attr.MAXLENGTH;
import static j2html.attributes.Attr.PATTERN;

/**
 * Utility class for generating HTML code for the Java Servlets that produce an HTML media type.
 *
 * @author Danny Damsky
 */
public final class HtmlPainter {

    public static final String GAME_INPUT_NAME_ONE = "one";
    public static final String GAME_INPUT_NAME_TWO = "two";
    public static final String GAME_INPUT_NAME_THREE = "three";
    public static final String GAME_INPUT_NAME_FOUR = "four";
    public static final String GAME_INPUT_NAME_FIVE = "five";
    public static final String GAME_INPUT_NAME_SIX = "six";
    public static final String GAME_INPUT_NAME_SEVEN = "seven";
    public static final String GAME_INPUT_NAME_EIGHT = "eight";
    public static final String GAME_INPUT_NAME_NINE = "nine";
    public static final String GAME_INPUT_NAME_TEN = "ten";

    private static final String[] GAME_INPUT_NAMES = new String[]{
            GAME_INPUT_NAME_ONE, GAME_INPUT_NAME_TWO, GAME_INPUT_NAME_THREE, GAME_INPUT_NAME_FOUR, GAME_INPUT_NAME_FIVE,
            GAME_INPUT_NAME_SIX, GAME_INPUT_NAME_SEVEN, GAME_INPUT_NAME_EIGHT, GAME_INPUT_NAME_NINE, GAME_INPUT_NAME_TEN
    };

    public static final String QUESTION_INPUT_NAME_QUESTION = "question";
    public static final String QUESTION_INPUT_NAME_REAL_ANSWER = "real";
    public static final String QUESTION_INPUT_NAME_FIRST_FAKE_ANSWER = "one";
    public static final String QUESTION_INPUT_NAME_SECOND_FAKE_ANSWER = "two";
    public static final String QUESTION_INPUT_NAME_THIRD_FAKE_ANSWER = "three";

    public static final String LOGIN_INPUT_NAME_EMAIL = "email";
    public static final String LOGIN_INPUT_NAME_USER = "user";
    public static final String LOGIN_INPUT_NAME_PASSWORD = "pass";
    public static final String LOGIN_INPUT_NAME_LOGOUT = "logout";

    private static final String ID_DOCS_LINK = "documentationLink";
    private static final String ID_LOGIN_LINK = "loginLink";
    private static final String ID_HEADER = "header";
    private static final String ID_HEADER_NAV = "headerNav";
    private static final String ID_MAIN_TITLE = "mainTitle";
    private static final String ID_MAIN_PAGE_LINK = "mainPageLink";
    private static final String ID_PLAY_GAME_BUTTON = "playGameButton";
    private static final String ID_ADD_QUESTION_BUTTON = "addQuestionButton";
    private static final String ID_BUTTON_LINK = "buttonLink";
    private static final String ID_FOOTER_NAV_CREDITS = "footerNavCredits";
    private static final String ID_SOCIAL_CONTAINER = "socialContainer";
    private static final String ID_FOOTER_NAV = "footerNav";
    private static final String ID_FOOTER = "footer";
    private static final String ID_LINKEDIN_LINK = "linkedInLink";
    private static final String ID_LINKEDIN_ICON = "linkedInIcon";
    private static final String ID_BITBUCKET_ICON = "bitbucketIcon";
    private static final String ID_BITBUCKET_LINK = "bitbucketLink";
    private static final String ID_MAIN = "main";
    private static final String ID_PLAY_BADGE = "playBadge";
    private static final String ID_PLAY_BADGE_CONTAINER = "playBadgeContainer";
    private static final String ID_RESET_BUTTON = "resetButton";
    private static final String ID_SUBMIT_BUTTON = "submitButton";
    private static final String ID_MAIN_SUBTITLE = "mainSubtitle";
    private static final String ID_ADD_QUESTION_FORM = "addQuestionForm";
    private static final String ID_BACKWARDS_LINK = "backwardsLink";
    private static final String ID_QUESTION_FORM = "questionForm";
    private static final String ID_REGISTER_BUTTON = "registerButton";
    private static final String ID_LOGIN_BUTTON = "loginButton";
    private static final String ID_LOGIN_FORM = "loginForm";
    private static final String ID_REGISTER_FORM = "registerForm";
    private static final String ID_OTHER_TITLE = "otherTitle";

    private static final String CLASS_DECOR_LESS_LINK = "decorationLessLink";
    private static final String CLASS_BIG_BUTTON = "bigButton";
    private static final String CLASS_CONTENT_CONTAINER = "contentContainer";
    private static final String CLASS_SOCIAL_IMAGE = "socialImage";

    private static final String HREF_DOCS = "/tqdb/web/docs";
    private static final String HREF_LOGIN = "/tqdb/web/login";
    private static final String HREF_PLAY = "/tqdb/web/play";
    private static final String HREF_ADD = "/tqdb/web/add";
    private static final String HREF_INDEX = "/tqdb/web";
    private static final String HREF_SUBMIT_GAME = "/tqdb/web/submit/game";
    private static final String HREF_SUBMIT_QUESTION = "/tqdb/web/submit/question";
    private static final String HREF_LINKEDIN = "https://www.linkedin.com/in/danny-damsky/";
    private static final String HREF_PLAY_STORE = "https://play.google.com/store/apps/details?id=com.damsky.danny.tqdb";
    private static final String HREF_BITBUCKET = "https://bitbucket.org/dannydamsky/";

    private static final String SRC_PLAY_BADGE = "/tqdb/images/playbadge.png";
    private static final String SRC_ICON = "/tqdb/images/icon.ico";
    private static final String SRC_LINKEDIN = "/tqdb/images/linkedin.svg";
    private static final String SRC_BITBUCKET = "/tqdb/images/bitbucket.svg";

    private static final String TARGET_BLANK = "_blank";

    private static final String TYPE_RESET = "reset";
    private static final String TYPE_SUBMIT = "submit";
    private static final String TYPE_RADIO = "radio";
    private static final String TYPE_TEXT = "text";
    private static final String TYPE_PASSWORD = "password";
    private static final String TYPE_EMAIL = "email";
    private static final String TYPE_HIDDEN = "hidden";

    private static final String METHOD_POST = "post";

    private static final String CSS_INDEX = "/tqdb/css/index.css";
    private static final String CSS_ADD = "/tqdb/css/add.css";
    private static final String CSS_LOGIN = "/tqdb/css/login.css";
    private static final String CSS_PLAY = "/tqdb/css/play.css";
    private static final String CSS_SUBMIT = "/tqdb/css/submit.css";
    private static final String CSS_USER = "/tqdb/css/user.css";
    private static final String CSS_DOCS = "/tqdb/css/doc.css";

    private static final ContainerTag TITLE = title("The Questionnaire Database - The internet's open trivia database");
    private static final EmptyTag META = meta().withContent("text/html").withCharset("UTF-8");
    private static final EmptyTag ICON = link().withRel("shortcut icon").withHref(SRC_ICON);
    private static final EmptyTag CSS = link().withRel("stylesheet");
    private static final ContainerTag A_DOCS = a("View our API documentation").withHref(HREF_DOCS).withId(ID_DOCS_LINK)
            .withClass(CLASS_DECOR_LESS_LINK);
    private static final ContainerTag A_TITLE = a(h1("The Questionnaire Database").withId(ID_MAIN_TITLE))
            .withHref(HREF_INDEX).withId(ID_MAIN_PAGE_LINK).withClass(CLASS_DECOR_LESS_LINK);

    private static final ContainerTag FOOTER = footer(
            nav(
                    p("This website has been developed by ")
                            .with(a("Danny Damsky").withHref(HREF_LINKEDIN).withStyle("color: white").withTarget(TARGET_BLANK))
                            .withId(ID_FOOTER_NAV_CREDITS),
                    div(
                            a(
                                    img().withId(ID_BITBUCKET_ICON).withSrc(SRC_BITBUCKET).withClass(CLASS_SOCIAL_IMAGE).withAlt("View my projects on Bitbucket")
                            ).withId(ID_BITBUCKET_LINK).withHref(HREF_BITBUCKET).withClass(CLASS_DECOR_LESS_LINK).withTarget(TARGET_BLANK),
                            a(
                                    img().withId(ID_LINKEDIN_ICON).withSrc(SRC_LINKEDIN).withClass(CLASS_SOCIAL_IMAGE).withAlt("Find me on LinkedIn")
                            ).withId(ID_LINKEDIN_LINK).withHref(HREF_LINKEDIN).withClass(CLASS_DECOR_LESS_LINK).withTarget(TARGET_BLANK)
                    ).withId(ID_SOCIAL_CONTAINER)
            ).withId(ID_FOOTER_NAV)
    ).withId(ID_FOOTER);


    private static final ContainerTag BUTTON_CLEAR = button("Clear").withId(ID_RESET_BUTTON)
            .withClass(CLASS_BIG_BUTTON).withType(TYPE_RESET);
    private static final ContainerTag BUTTON_SUBMIT = button("Submit").withId(ID_SUBMIT_BUTTON)
            .withClass(CLASS_BIG_BUTTON).withType(TYPE_SUBMIT);

    private static ContainerTag constructHeadTag(final String cssFilePath) {
        return head(TITLE, META, ICON, CSS.withHref(cssFilePath));
    }

    private static ContainerTag constructHeaderTag(final String loginInfo) {
        return header(
                nav(
                        A_DOCS,
                        a(loginInfo).withHref(HREF_LOGIN).withId(ID_LOGIN_LINK).withClass(CLASS_DECOR_LESS_LINK)
                ).withId(ID_HEADER_NAV)
        ).withId(ID_HEADER);
    }

    private static ContainerTag constructFieldSet(final String legend, final String name, final String placeHolder) {
        return fieldset(
                legend(legend),
                input().withType(TYPE_TEXT).attr(MAXLENGTH, 255).withName(name).withPlaceholder(placeHolder).isRequired()
        );
    }

    /**
     * @param loginInfo either "Log in" or the user's username.
     * @return Generated HTML code of the welcome page.
     */
    public static String paintWelcomePage(final String loginInfo) {
        return html(
                constructHeadTag(CSS_INDEX),
                body(constructHeaderTag(loginInfo),
                        main(
                                div(
                                        A_TITLE,
                                        a(
                                                button(
                                                        "Play the game"
                                                ).withClass(CLASS_BIG_BUTTON).withId(ID_PLAY_GAME_BUTTON)
                                        ).withHref(HREF_PLAY).withId(ID_BUTTON_LINK),
                                        a(
                                                button(
                                                        "Add your own question"
                                                ).withClass(CLASS_BIG_BUTTON).withId(ID_ADD_QUESTION_BUTTON)
                                        ).withHref(HREF_ADD).withId(ID_BUTTON_LINK)
                                ).withClass(CLASS_CONTENT_CONTAINER),
                                a(
                                        img().withId(ID_PLAY_BADGE).withSrc(SRC_PLAY_BADGE).withStyle("object-fit: scale-down;").withAlt("")
                                ).withHref(HREF_PLAY_STORE)
                                        .withTarget(TARGET_BLANK).withId(ID_PLAY_BADGE_CONTAINER)
                        ).withId(ID_MAIN), FOOTER
                )
        ).render();
    }

    /**
     * @param loginInfo either "Log in" or the user's username.
     * @return Generated HTML code of the add page.
     */
    public static String paintAddPage(final String loginInfo) {
        return html(
                constructHeadTag(CSS_ADD),
                body(constructHeaderTag(loginInfo),
                        main(
                                div(
                                        A_TITLE,
                                        h2("Add your own trivia question to the site").withId(ID_MAIN_SUBTITLE),
                                        form(
                                                constructFieldSet("The question", QUESTION_INPUT_NAME_QUESTION, "Enter the question here"),
                                                constructFieldSet("The real answer to the question", QUESTION_INPUT_NAME_REAL_ANSWER, "Enter the real answer here"),
                                                constructFieldSet("1st fake answer", QUESTION_INPUT_NAME_FIRST_FAKE_ANSWER, "Enter the 1st fake answer here"),
                                                constructFieldSet("2nd fake answer", QUESTION_INPUT_NAME_SECOND_FAKE_ANSWER, "Enter the 2nd fake answer here"),
                                                constructFieldSet("3rd fake answer", QUESTION_INPUT_NAME_THIRD_FAKE_ANSWER, "Enter the 3rd fake answer here"),
                                                BUTTON_CLEAR,
                                                BUTTON_SUBMIT
                                        ).withId(ID_ADD_QUESTION_FORM).withAction(HREF_SUBMIT_QUESTION).withMethod(METHOD_POST)
                                ).withClass(CLASS_CONTENT_CONTAINER)
                        ).withId(ID_MAIN), FOOTER
                )
        ).render();
    }


    /**
     * @param loginInfo either "Log in" or the user's username.
     * @param questions 10 questions for the user to answer
     * @return Generated HTML code of the play page.
     */
    public static String paintPlayPage(final String loginInfo, final List<Question> questions) {
        return html(
                constructHeadTag(CSS_PLAY),
                body(constructHeaderTag(loginInfo),
                        main(
                                div(
                                        A_TITLE,
                                        h2("Answer the following questions").withId(ID_MAIN_SUBTITLE),
                                        form(
                                                getQuestionField(questions.get(0), 0),
                                                getQuestionField(questions.get(1), 1),
                                                getQuestionField(questions.get(2), 2),
                                                getQuestionField(questions.get(3), 3),
                                                getQuestionField(questions.get(4), 4),
                                                getQuestionField(questions.get(5), 5),
                                                getQuestionField(questions.get(6), 6),
                                                getQuestionField(questions.get(7), 7),
                                                getQuestionField(questions.get(8), 8),
                                                getQuestionField(questions.get(9), 9),
                                                BUTTON_CLEAR,
                                                BUTTON_SUBMIT
                                        ).withId(ID_QUESTION_FORM).withAction(HREF_SUBMIT_GAME).withMethod(METHOD_POST)
                                ).withClass(CLASS_CONTENT_CONTAINER)
                        ).withId(ID_MAIN)), FOOTER
        ).render();
    }

    private static ContainerTag getQuestionField(final Question question, final int index) {
        final String name = GAME_INPUT_NAMES[index];
        final ContainerTag legend = legend("Question " + (index + 1) + ": " + question.getQuestion());
        final ContainerTag realLabel = label(question.getRealAnswer());
        final ContainerTag firstFakeLabel = label(question.getFirstFakeAnswer());
        final ContainerTag secondFakeLabel = label(question.getSecondFakeAnswer());
        final ContainerTag thirdFakeLabel = label(question.getThirdFakeAnswer());
        final EmptyTag realInput = input().withType(TYPE_RADIO).withName(name).withValue(question.getRealAnswer()).isRequired();
        final EmptyTag firstFakeInput = input().withType(TYPE_RADIO).withName(name).withValue(question.getFirstFakeAnswer()).isRequired();
        final EmptyTag secondFakeInput = input().withType(TYPE_RADIO).withName(name).withValue(question.getSecondFakeAnswer()).isRequired();
        final EmptyTag thirdFakeInput = input().withType(TYPE_RADIO).withName(name).withValue(question.getThirdFakeAnswer()).isRequired();

        final Random random = RandomUtils.getRandom();
        final int randomNum = random.nextInt(3) + 1;
        switch (randomNum) {
            case 1:
                return fieldset(legend, realLabel, realInput, firstFakeLabel, firstFakeInput, secondFakeLabel, secondFakeInput,
                        thirdFakeLabel, thirdFakeInput);
            case 2:
                return fieldset(legend, firstFakeLabel, firstFakeInput, realLabel, realInput, secondFakeLabel, secondFakeInput,
                        thirdFakeLabel, thirdFakeInput);
            case 3:
                return fieldset(legend, firstFakeLabel, firstFakeInput, secondFakeLabel, secondFakeInput, realLabel, realInput,
                        thirdFakeLabel, thirdFakeInput);
            case 4:
                return fieldset(legend, firstFakeLabel, firstFakeInput, secondFakeLabel, secondFakeInput, thirdFakeLabel, thirdFakeInput,
                        realLabel, realInput);
            default:
                return fieldset(legend, realLabel, realInput, firstFakeLabel, firstFakeInput, secondFakeLabel, secondFakeInput,
                        thirdFakeLabel, thirdFakeInput);
        }
    }

    /**
     * @return Generated HTML code of the submit page.
     */
    public static String paintSubmitQuestionPage() {
        return html(
                constructHeadTag(CSS_SUBMIT),
                body(
                        main(
                                div(
                                        A_TITLE,
                                        h2("You question has been submitted for review!").withId(ID_MAIN_SUBTITLE),
                                        a("Click here to add another question!").withId(ID_BACKWARDS_LINK).withHref(HREF_ADD)
                                ).withClass(CLASS_CONTENT_CONTAINER)
                        ).withId(ID_MAIN)
                )
        ).render();
    }

    /**
     * @param numOutOfTen How many answers (out of 10) were correct.
     * @return Generated HTML code of the submit game page.
     */
    public static String paintSubmitGamePage(final int numOutOfTen) {
        return html(
                constructHeadTag(CSS_SUBMIT),
                body(
                        main(
                                div(
                                        A_TITLE,
                                        h2("Congratulations! You've answered " + numOutOfTen + " / 10 answers correctly!").withId(ID_MAIN_SUBTITLE),
                                        a("Click here to play another game!").withId(ID_BACKWARDS_LINK).withHref(HREF_PLAY)
                                ).withClass(CLASS_CONTENT_CONTAINER)
                        ).withId(ID_MAIN)
                )
        ).render();
    }

    /**
     * @return Generated HTML code of the login page.
     */
    public static String paintLoginPage() {
        return html(
                constructHeadTag(CSS_LOGIN),
                body(
                        main(
                                div(
                                        A_TITLE,
                                        h2("Enter your details to login").withId(ID_MAIN_SUBTITLE),
                                        form(
                                                fieldset(
                                                        legend("Login"),
                                                        label("Username"),
                                                        input().attr(PATTERN, AuthUtils.REGEX_USERNAME_VALIDATION).withType(TYPE_TEXT).withName(LOGIN_INPUT_NAME_USER).withPlaceholder("Enter your username or e-mail").isRequired(),
                                                        label("Password"),
                                                        input().attr(PATTERN, AuthUtils.REGEX_PASSWORD_VALIDATION).withType(TYPE_PASSWORD).withName(LOGIN_INPUT_NAME_PASSWORD).withPlaceholder("Enter your password").isRequired(),
                                                        BUTTON_CLEAR,
                                                        button("Log in").withId(ID_LOGIN_BUTTON).withClass(CLASS_BIG_BUTTON).withType(TYPE_SUBMIT)
                                                )
                                        ).withId(ID_LOGIN_FORM).withAction(HREF_INDEX).withMethod(METHOD_POST),
                                        form(
                                                fieldset(
                                                        legend("Register"),
                                                        label("E-Mail Address"),
                                                        input().withType(TYPE_EMAIL).withName(LOGIN_INPUT_NAME_EMAIL).withPlaceholder("Enter your e-mail address").isRequired(),
                                                        label("Username"),
                                                        input().attr(PATTERN, AuthUtils.REGEX_USERNAME_VALIDATION).withType(TYPE_TEXT).withName(LOGIN_INPUT_NAME_USER).withPlaceholder("Enter your username").isRequired(),
                                                        label("Password"),
                                                        input().attr(PATTERN, AuthUtils.REGEX_PASSWORD_VALIDATION).withType(TYPE_PASSWORD).withName(LOGIN_INPUT_NAME_PASSWORD).withPlaceholder("Enter your password").isRequired(),
                                                        BUTTON_CLEAR,
                                                        button("Register").withId(ID_REGISTER_BUTTON).withClass(CLASS_BIG_BUTTON).withType(TYPE_SUBMIT)
                                                )
                                        ).withId(ID_REGISTER_FORM).withAction(HREF_INDEX).withMethod(METHOD_POST)
                                ).withClass(CLASS_CONTENT_CONTAINER)
                        ).withId(ID_MAIN)
                )
        ).render();
    }

    /**
     * @param user The user that's logged into the system.
     * @return Generated HTML code of the user's profile page.
     */
    public static String paintUserPage(final User user) {
        final List<Game> userGames = user.getGames();
        float score = 0;
        final int size = userGames.size();
        ContainerTag domContent = tag(null);
        if (size != 0) {
            for (int i = 0; i < size; i++) {
                final Game game = userGames.get(i);
                score += game.getScore();
                domContent = domContent.with(
                        tr(
                                td("" + (i + 1)),
                                td(game.getScore() + " / 10"),
                                td(DateUtils.format(game.getWhen()))
                        )
                );
            }
            score /= size;
        }
        return html(
                constructHeadTag(CSS_USER),
                body(
                        main(
                                div(
                                        A_TITLE,
                                        h2("Your username: " + user.getUsername()).withId(ID_MAIN_SUBTITLE),
                                        h3("Your average score: " + String.format("%.1f", score) + " / 10").withId(ID_OTHER_TITLE),
                                        h3("Your API key: " + user.getApiKey()).withId(ID_OTHER_TITLE),
                                        form(
                                                input().withType(TYPE_HIDDEN).withName(LOGIN_INPUT_NAME_LOGOUT).withValue(LOGIN_INPUT_NAME_LOGOUT),
                                                button("Log out").withType(TYPE_SUBMIT).withClass(CLASS_BIG_BUTTON)
                                        ).withMethod(METHOD_POST).withAction(HREF_LOGIN),
                                        hr().withStyle("width: 100%"),
                                        h3("Your game history").withId(ID_OTHER_TITLE),
                                        table(
                                                tr(
                                                        th("Game Number"),
                                                        th("Score"),
                                                        th("Date")
                                                ), domContent
                                        )
                                ).withClass(CLASS_CONTENT_CONTAINER)
                        ).withId(ID_MAIN)
                )
        ).render();
    }

    public static String paintDocsPage() {
        return html(
                constructHeadTag(CSS_DOCS),
                header(
                        nav(
                                a("Go back").withHref(HREF_INDEX).withClass(CLASS_DECOR_LESS_LINK),
                                a("Getting started").withHref("#api").withClass(CLASS_DECOR_LESS_LINK),
                                a("Questions").withHref("#questions").withClass(CLASS_DECOR_LESS_LINK),
                                a("Games").withHref("#games").withClass(CLASS_DECOR_LESS_LINK)
                        )
                ),
                body(
                        main(
                                h1("The Questionnaire Database - Documentation"),
                                h3("Welcome to the documentation page. Here you'll find how to use the public API in your applications.").withId(ID_MAIN_SUBTITLE),
                                section(
                                        h2("Section #1: Getting started"),
                                        p(join(
                                                "In order to retrieve questions from the API, you don't need to use an API key.",
                                                br(), "But if you want to use the API more extensively, for example: register users to the website and store the games they play in the database, you'll need an API key.",
                                                br(),
                                                br(), "There are two ways of retrieving the API key. One way is to register using the website. All you have to do in that case is just register and visit your profile page. There you'll find your API key.",
                                                br(),
                                                br(), "Another way to register is using the API. All you have to do is make a POST request to the following link:",
                                                br(),
                                                br(),
                                                code("http://ec2-18-222-152-255.us-east-2.compute.amazonaws.com/tqdb/web/login")
                                                )
                                        ),
                                        p("When making a POST request to the above URL, you must provide a JSON with the following properties"),
                                        ul(
                                                li(join(b("\"email\""), " - The user's email, must be an actual email address.")),
                                                li(join(b("\"username\""), "- The user's username, must have a length between 2 and 20 characters. Can't contain whitespace.")),
                                                li(join(b("\"password\""), "- The user's password, must be a strong password, 8-48 characters long, containing at least 1 special character, 1 uppercase letter, 1 lowercase letter, and 1 number."))
                                        ),
                                        p("The POST request will return a message back to the caller. If successful it will contain a JSON that looks like this (without the comments):"),
                                        p(join(
                                                "{",
                                                br(), "&emsp;\"result\": {",
                                                br(), "&emsp;&emsp;\"message\": \"SUCCESS\", // Or \"ERROR\"",
                                                br(), "&emsp;&emsp;\"explanation\": null // If the message is \"ERROR\" then there will be an explanation attached.",
                                                br(), "&emsp;},",
                                                br(), "&emsp;\"object\": {",
                                                br(), "&emsp;&emsp;\"id\": 1,",
                                                br(), "&emsp;&emsp;\"apiKey\": \"someApiKey\",",
                                                br(), "&emsp;&emsp;\"email\": \"some@email.com\",",
                                                br(), "&emsp;&emsp;\"username\": \"someusername\",",
                                                br(), "&emsp;&emsp;\"password\": \"somepassword\",",
                                                br(), "&emsp;&emsp;\"games\": null // The new user hasn't played any games yet.",
                                                br(), "&emsp;}",
                                                br(), "}"
                                                )
                                        )
                                ).withId("api"),
                                section(
                                        h2("Section #2: Questions"),
                                        p(join(
                                                "The questions are the essence of the website.",
                                                br(), "In order to submit questions or get questions you do not have to be logged into the application or use an API key."
                                        )),
                                        h3("Getting all questions"),
                                        p(join(
                                                "If all you need is to get questions from the database, all you need to do is use the following URL and provide a page number (starting from 0), each page contains up to 10 questions.",
                                                br(), br(),
                                                code("http://ec2-18-222-152-255.us-east-2.compute.amazonaws.com/tqdb/web/questions/confirmed-only/YOU_NUMBER_HERE")
                                        )),
                                        p(join(
                                                "This will return all the questions of the trivia site that have been confirmed by the admin to be valid. If you want to get all of the site's questions (including those which aren't confirmed and might potentially be removed in the future) you can use the following URL:",
                                                br(),
                                                br(),
                                                code("http://ec2-18-222-152-255.us-east-2.compute.amazonaws.com/tqdb/web/questions/all/YOU_NUMBER_HERE")
                                        )),
                                        p(join(
                                                "Doing this will return a JSON object that looks like this:",
                                                br(),
                                                br(), "{",
                                                br(), "&emsp;\"result\": {",
                                                br(), "&emsp;&emsp;\"message\": SUCCESS,",
                                                br(), "&emsp;&emsp;\"explanation\": null",
                                                br(), "&emsp;},",
                                                br(), "&emsp;\"object\": [",
                                                br(), "&emsp;&emsp;{",
                                                br(), "&emsp;&emsp;&emsp;\"id\": 1,",
                                                br(), "&emsp;&emsp;&emsp;\"question\": \"somequestion\",",
                                                br(), "&emsp;&emsp;&emsp;\"realAnswer\": \"someanswer\",",
                                                br(), "&emsp;&emsp;&emsp;\"firstFakeAnswer\": \"somefakeanswer1\",",
                                                br(), "&emsp;&emsp;&emsp;\"secondFakeAnswer\": \"somefakeanswer2\",",
                                                br(), "&emsp;&emsp;&emsp;\"thirdFakeAnswer\": \"somefakeanswer3\",",
                                                br(), "&emsp;&emsp;&emsp;\"confirmed\": true",
                                                br(), "&emsp;&emsp;},",
                                                br(), "&emsp;&emsp;...",
                                                br(), "&emsp;]",
                                                br(), "}"
                                        )),
                                        h3("Submitting your own questions"),
                                        p(join(
                                                "You can upload your own questions to the database using our API.",
                                                br(), "In order to do that you need to call a POST request to the URL below:",
                                                br(),
                                                br(),
                                                code("http://ec2-18-222-152-255.us-east-2.compute.amazonaws.com/tqdb/web/submit/question")
                                        )),
                                        p("In your call you must send a JSON with the following properties:"),
                                        ul(
                                                li(b("\"question\"")),
                                                li(b("\"realAnswer\"")),
                                                li(b("\"firstFakeAnswer\"")),
                                                li(b("\"secondFakeAnswer\"")),
                                                li(b("\"thirdFakeAnswer\""))
                                        ),
                                        p("The API returns a JSON message indicating if it succeeded or failed.")
                                ).withId("questions"),
                                section(
                                        h2("Section #3: Games"),
                                        h3("Submitting your game"),
                                        p(join(
                                                "You can submit the user's games using this API to save the user's progress.",
                                                br(), "In order to do that you must first obtain an ",
                                                a("API key").withHref("#api"), ".",
                                                br(),
                                                br(), "If you've already obtained an API key, in order to submit a game you need to use the following URL:",
                                                br(),
                                                br(),
                                                code("http://ec2-18-222-152-255.us-east-2.compute.amazonaws.com/tqdb/web/submit/game?key=YOUR_API_KEY")
                                        )),
                                        p("When creating a POST request to the above URL, you must supply a JSON containing game info:"),
                                        ul(
                                                li(join(
                                                        b("\"score\""), " - How many questions the user answered correctly (0-10)")),
                                                li(join(
                                                        b("\"when\""), " - The time in milliseconds from when the game ended."
                                                ))
                                        ),
                                        h3("Getting your games"),
                                        p(join(
                                                "You can get your own games by using the following URL:",
                                                br(),
                                                br(),
                                                code("http://ec2-18-222-152-255.us-east-2.compute.amazonaws.com/tqdb/web/games?key=YOU_API_KEY")
                                        ))
                                ).withId("games"),
                                h3("Thank you for using this API").withId("thanks")
                        )
                )
        ).render();
    }

    private HtmlPainter() {
    }
}
