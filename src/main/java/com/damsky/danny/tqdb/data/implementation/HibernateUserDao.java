package com.damsky.danny.tqdb.data.implementation;

import com.damsky.danny.tqdb.data.AppDatabase;
import com.damsky.danny.tqdb.data.DbConstants;
import com.damsky.danny.tqdb.data.dao.UserDao;
import com.damsky.danny.tqdb.data.entity.Game;
import com.damsky.danny.tqdb.data.entity.User;
import com.damsky.danny.tqdb.util.AuthUtils;
import org.hibernate.Session;

import java.util.List;

/**
 * {@link UserDao} implementation using Hibernate ORM.
 *
 * @author Danny Damsky
 * @see User
 * @see UserDao
 */
public final class HibernateUserDao implements UserDao {

    @Override
    public void insert(User user) {
        appDatabase.execute(currentSession ->
                currentSession.save(user)
        );
    }

    @Override
    public void delete(User user) {
        appDatabase.execute(currentSession ->
                currentSession.delete(user)
        );
    }

    @Override
    public void update(User user) {
        appDatabase.execute(currentSession ->
                currentSession.update(user)
        );
    }

    @Override
    public List<User> getAll() {
        //noinspection unchecked
        return appDatabase.executeAndReturn((AppDatabase.DatabaseQueryCommunicator<List<User>>) currentSession ->
                currentSession.createSQLQuery("SELECT * FROM " + DbConstants.USERS_TABLE_NAME + ";")
                        .addEntity(User.class)
                        .list()
        );
    }

    @Override
    public User getByApiKey(String apiKey) {
        return appDatabase.executeAndReturn(currentSession ->
                queryForUserByApiKey(currentSession, apiKey)
        );
    }

    private User queryForUserByApiKey(Session currentSession, String apiKey) {
        return (User) currentSession.createSQLQuery("SELECT * FROM " + DbConstants.USERS_TABLE_NAME +
                " WHERE " + DbConstants.USERS_COLUMN_API_KEY + "=\"" + apiKey + "\";")
                .addEntity(User.class).uniqueResult();
    }

    @Override
    public User getByEmailOrUsernameAndPassword(String usernameOrEmail, String password) {
        if (AuthUtils.isValidEmail(usernameOrEmail)) {
            return getByEmailAndPassword(usernameOrEmail, password);
        }
        return getByUsernameAndPassword(usernameOrEmail, password);
    }

    private User getByEmailAndPassword(String email, String password) {
        return appDatabase.executeAndReturn(currentSession ->
                queryForUserByEmail(currentSession, email, password)
        );
    }

    private User queryForUserByEmail(Session currentSession, String email, String password) {
        return (User) currentSession.createSQLQuery("SELECT * FROM " + DbConstants.USERS_TABLE_NAME +
                " WHERE " + DbConstants.USERS_COLUMN_EMAIL + "=\"" + email +
                "\" AND " + DbConstants.USERS_COLUMN_PASSWORD + "=\"" + password + "\";")
                .addEntity(User.class).uniqueResult();
    }

    private User getByUsernameAndPassword(String username, String password) {
        return appDatabase.executeAndReturn(currentSession ->
                queryForUserByUsername(currentSession, username, password)
        );
    }

    private User queryForUserByUsername(Session currentSession, String username, String password) {
        return (User) currentSession.createSQLQuery("SELECT * FROM " + DbConstants.USERS_TABLE_NAME +
                " WHERE " + DbConstants.USERS_COLUMN_USERNAME + "=\"" + username +
                "\" AND " + DbConstants.USERS_COLUMN_PASSWORD + "=\"" + password + "\";")
                .addEntity(User.class).uniqueResult();
    }

    @Override
    public void addGameToUserByApiKey(String apiKey, Game game) {
        appDatabase.execute(currentSession -> {
            final User user = queryForUserByApiKey(currentSession, apiKey);
            addNewGameToUser(currentSession, user, game);
        });
    }

    private void addNewGameToUser(Session currentSession, User user, Game game) {
        game.setUser(user);
        user.getGames().add(game);
        currentSession.update(user);
    }
}
