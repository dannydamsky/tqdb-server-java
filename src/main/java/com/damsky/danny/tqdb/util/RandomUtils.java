package com.damsky.danny.tqdb.util;

import java.util.Random;

/**
 * Utility class for functions associated with the {@link Random} class.
 *
 * @author Danny Damsky
 */
final class RandomUtils {
    private static final Random RANDOM = new Random();

    static Random getRandom() {
        return RANDOM;
    }

    static Random getRandomAggressive() {
        return new Random(System.nanoTime());
    }
}
