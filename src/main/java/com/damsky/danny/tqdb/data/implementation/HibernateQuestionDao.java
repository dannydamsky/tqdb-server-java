package com.damsky.danny.tqdb.data.implementation;

import com.damsky.danny.tqdb.data.AppDatabase;
import com.damsky.danny.tqdb.data.DbConstants;
import com.damsky.danny.tqdb.data.dao.QuestionDao;
import com.damsky.danny.tqdb.data.entity.Question;

import java.util.List;

/**
 * {@link QuestionDao} implementation using Hibernate ORM.
 *
 * @author Danny Damsky
 * @see Question
 * @see QuestionDao
 */
public final class HibernateQuestionDao implements QuestionDao {
    @Override
    public void insert(final Question question) {
        appDatabase.execute(currentSession ->
                currentSession.save(question)
        );
    }

    @Override
    public void delete(final Question question) {
        appDatabase.execute(currentSession ->
                currentSession.delete(question)
        );
    }

    @Override
    public void update(final Question question) {
        appDatabase.execute(currentSession ->
                currentSession.update(question)
        );
    }

    @Override
    public List<Question> getAll() {
        //noinspection unchecked
        return appDatabase.executeAndReturn((AppDatabase.DatabaseQueryCommunicator<List<Question>>) currentSession ->
                currentSession.createSQLQuery("SELECT * FROM " + DbConstants.QUESTIONS_TABLE_NAME + ";")
                        .addEntity(Question.class)
                        .list()
        );
    }

    @Override
    public List<Question> getAllConfirmed() {
        //noinspection unchecked
        return appDatabase.executeAndReturn((AppDatabase.DatabaseQueryCommunicator<List<Question>>) currentSession ->
                currentSession.createSQLQuery("SELECT * FROM " + DbConstants.QUESTIONS_TABLE_NAME + " WHERE " +
                        DbConstants.QUESTIONS_COLUMN_CONFIRMED + "=1" + ";")
                        .addEntity(Question.class)
                        .list()
        );
    }

    @Override
    public List<Question> getAllFromTo(int rowFrom, int rows) {
        //noinspection unchecked
        return appDatabase.executeAndReturn((AppDatabase.DatabaseQueryCommunicator<List<Question>>) currentSession ->
                currentSession.createSQLQuery("SELECT * FROM " + DbConstants.QUESTIONS_TABLE_NAME +
                        " LIMIT " + rowFrom + "," + rows + ";")
                        .addEntity(Question.class)
                        .list()
        );
    }

    @Override
    public List<Question> getAllFromToConfirmed(int rowFrom, int rows) {
        //noinspection unchecked
        return appDatabase.executeAndReturn((AppDatabase.DatabaseQueryCommunicator<List<Question>>) currentSession ->
                currentSession.createSQLQuery("SELECT * FROM " + DbConstants.QUESTIONS_TABLE_NAME + " WHERE " +
                        DbConstants.QUESTIONS_COLUMN_CONFIRMED + "=1" + " LIMIT " + rowFrom + "," + rows + ";")
                        .addEntity(Question.class)
                        .list()
        );
    }

    @Override
    public List<Question> getRandom(int count) {
        //noinspection unchecked
        return appDatabase.executeAndReturn((AppDatabase.DatabaseQueryCommunicator<List<Question>>) currentSession ->
                currentSession.createSQLQuery("SELECT * FROM " + DbConstants.QUESTIONS_TABLE_NAME +
                        " ORDER BY RAND() LIMIT " + count + ";")
                        .addEntity(Question.class)
                        .list()
        );
    }

    @Override
    public List<Question> getRandomConfirmed(int count) {
        //noinspection unchecked
        return appDatabase.executeAndReturn((AppDatabase.DatabaseQueryCommunicator<List<Question>>) currentSession ->
                currentSession.createSQLQuery("SELECT * FROM " + DbConstants.QUESTIONS_TABLE_NAME + " WHERE " +
                        DbConstants.QUESTIONS_COLUMN_CONFIRMED + "=1" + " ORDER BY RAND() LIMIT " + count + ";")
                        .addEntity(Question.class)
                        .list()
        );
    }
}
