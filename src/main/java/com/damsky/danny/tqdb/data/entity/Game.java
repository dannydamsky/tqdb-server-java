package com.damsky.danny.tqdb.data.entity;

import com.damsky.danny.tqdb.data.DbConstants;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A class representing the games table in the database.
 *
 * @author Danny Damsky
 * @see User User - The users table is in a 1-N relationship with games.
 * @see Question
 */
@Entity
@Table(name = DbConstants.GAMES_TABLE_NAME)
public final class Game implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbConstants.GAMES_COLUMN_ID, nullable = false)
    private long id;
    @Column(name = DbConstants.GAMES_COLUMN_SCORE, nullable = false)
    private int score;
    @Column(name = DbConstants.GAMES_COLUMN_WHEN, nullable = false, unique = true)
    private long when;
    @ManyToOne
    @JoinColumn(name = DbConstants.GAMES_COLUMN_USER_ID, nullable = false)
    private User user;

    private Game(Builder builder) {
        this.id = builder.id;
        this.score = builder.score;
        this.when = builder.when;
        this.user = builder.user;
    }

    public Game() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getWhen() {
        return when;
    }

    public void setWhen(long when) {
        this.when = when;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", score=" + score +
                ", when=" + when +
                '}';
    }

    public static final class Builder {
        private long id;
        private int score;
        private long when;
        private User user;

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setScore(int score) {
            this.score = score;
            return this;
        }

        public Builder setWhen(long when) {
            this.when = when;
            return this;
        }

        public Builder setUser(User user) {
            this.user = user;
            return this;
        }

        public Game build() {
            return new Game(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "id=" + id +
                    ", score=" + score +
                    ", when=" + when +
                    '}';
        }
    }
}
