package com.damsky.danny.tqdb.util.json;

import com.damsky.danny.tqdb.data.entity.Question;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A lightweight version of the {@link Question} class.
 * It's used for getting and passing JSONs using the API.
 *
 * @author Danny Damsky
 */
@XmlRootElement
public final class JsonQuestion {

    @JsonProperty("question")
    private String question;
    @JsonProperty("realAnswer")
    private String realAnswer;
    @JsonProperty("firstFakeAnswer")
    private String firstFakeAnswer;
    @JsonProperty("secondFakeAnswer")
    private String secondFakeAnswer;
    @JsonProperty("thirdFakeAnswer")
    private String thirdFakeAnswer;

    // Reserved for Jackson JSON Parser
    public JsonQuestion() {
    }

    /**
     * @return Makes sure that the object's fields are valid and if they are
     * then it returns a {@link Question} object with the same fields, if not, returns null.
     */
    public Question validateAndConvert() {
        if (validateStrings(question, realAnswer, firstFakeAnswer, secondFakeAnswer, thirdFakeAnswer)) {
            return new Question.Builder()
                    .setQuestion(question)
                    .setRealAnswer(realAnswer)
                    .setFirstFakeAnswer(firstFakeAnswer)
                    .setSecondFakeAnswer(secondFakeAnswer)
                    .setThirdFakeAnswer(thirdFakeAnswer)
                    .build();
        }
        return null;
    }

    private boolean validateStrings(final String... strings) {
        for (final String string : strings) {
            if (string == null || string.trim().isEmpty() || string.length() > 255) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "JsonQuestion{" +
                "question='" + question + '\'' +
                ", realAnswer='" + realAnswer + '\'' +
                ", firstFakeAnswer='" + firstFakeAnswer + '\'' +
                ", secondFakeAnswer='" + secondFakeAnswer + '\'' +
                ", thirdFakeAnswer='" + thirdFakeAnswer + '\'' +
                '}';
    }
}
