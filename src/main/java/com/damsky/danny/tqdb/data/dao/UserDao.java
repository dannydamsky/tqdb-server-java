package com.damsky.danny.tqdb.data.dao;

import com.damsky.danny.tqdb.data.entity.Game;
import com.damsky.danny.tqdb.data.entity.User;

/**
 * Interface for implementing DAO classes of type {@link User}.
 *
 * @author Danny Damsky
 * @see Dao
 * @see User
 */
public interface UserDao extends Dao<User> {

    User getByApiKey(final String apiKey);

    User getByEmailOrUsernameAndPassword(final String usernameOrEmail, final String password);

    void addGameToUserByApiKey(final String apiKey, final Game game);
}
