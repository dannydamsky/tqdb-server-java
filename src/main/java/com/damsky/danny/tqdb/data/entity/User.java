package com.damsky.danny.tqdb.data.entity;

import com.damsky.danny.tqdb.data.DbConstants;
import com.damsky.danny.tqdb.util.AuthUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * A class representing the users table in the database.
 *
 * @author Danny Damsky
 * @see Game Game - The users table is in a 1-N relationship with games.
 * @see Question
 */
@Entity
@Table(name = DbConstants.USERS_TABLE_NAME)
public final class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbConstants.USERS_COLUMN_ID, nullable = false)
    private long id;
    @Column(name = DbConstants.USERS_COLUMN_API_KEY, nullable = false, unique = true)
    private String apiKey;
    @Column(name = DbConstants.USERS_COLUMN_EMAIL, nullable = false, unique = true)
    private String email;
    @Column(name = DbConstants.USERS_COLUMN_USERNAME, nullable = false, unique = true)
    private String username;
    @Column(name = DbConstants.USERS_COLUMN_PASSWORD, nullable = false)
    private String password;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Game> games;

    private User(Builder builder) {
        this.id = builder.id;
        this.apiKey = builder.apiKey;
        this.email = builder.email;
        this.username = builder.username;
        this.password = builder.password;
        this.games = builder.games;
    }

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", apiKey='" + apiKey + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public static final class Builder {
        private long id;
        private String apiKey;
        private String email;
        private String username;
        private String password;
        private List<Game> games;

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setApiKey(String apiKey) {
            this.apiKey = apiKey;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setGames(List<Game> games) {
            this.games = games;
            return this;
        }

        public User build() {
            if (this.apiKey == null) {
                this.apiKey = AuthUtils.generateKey();
            }
            return new User(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "id=" + id +
                    ", apiKey='" + apiKey + '\'' +
                    ", email='" + email + '\'' +
                    ", username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }
}
