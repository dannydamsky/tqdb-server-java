package com.damsky.danny.tqdb.util.json;

import com.damsky.danny.tqdb.data.entity.Game;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A lightweight version of the {@link Game} class.
 * It's used for getting and passing JSONs using the API.
 *
 * @author Danny Damsky
 */
@XmlRootElement
public final class JsonGame {

    @JsonProperty("score")
    private int score;
    @JsonProperty("when")
    private long when;

    // Reserved for Jackson JSON Parser
    public JsonGame() {
    }

    public JsonGame(int score, long when) {
        this.score = score;
        this.when = when;
    }

    /**
     * @return Makes sure that the object's fields are valid and if they are
     * then it returns a {@link Game} object with the same fields, if not, returns null.
     */
    public Game validateAndConvert() {
        final long delta = System.currentTimeMillis() - when;
        if (score >= 0 && score <= 10 && delta >= 0 && delta <= 1_800_000) {
            return new Game.Builder().setScore(score).setWhen(when).build();
        }
        return null;
    }

    @Override
    public String toString() {
        return "JsonGame{" +
                "score=" + score +
                ", when=" + when +
                '}';
    }
}
