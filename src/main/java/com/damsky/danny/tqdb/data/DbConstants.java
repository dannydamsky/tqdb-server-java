package com.damsky.danny.tqdb.data;

/**
 * Constants that are used in the actual database, such as table names and columns.
 *
 * @author Danny Damsky
 */
public final class DbConstants {

    public static final String GAMES_TABLE_NAME = "GAMES";
    public static final String GAMES_COLUMN_ID = "GAME_ID";
    public static final String GAMES_COLUMN_SCORE = "GAME_SCORE";
    public static final String GAMES_COLUMN_WHEN = "GAME_WHEN";
    public static final String GAMES_COLUMN_USER_ID = "USER_ID";

    public static final String USERS_TABLE_NAME = "USERS";
    public static final String USERS_COLUMN_ID = "USER_ID";
    public static final String USERS_COLUMN_API_KEY = "USER_API_KEY";
    public static final String USERS_COLUMN_EMAIL = "USER_EMAIL";
    public static final String USERS_COLUMN_USERNAME = "USER_USERNAME";
    public static final String USERS_COLUMN_PASSWORD = "USER_PASSWORD";

    public static final String QUESTIONS_TABLE_NAME = "QUESTIONS";
    public static final String QUESTIONS_COLUMN_ID = "QUESTION_ID";
    public static final String QUESTIONS_COLUMN_QUESTION = "QUESTION_QUESTION";
    public static final String QUESTIONS_COLUMN_REAL_ANSWER = "QUESTION_REAL_ANSWER";
    public static final String QUESTIONS_COLUMN_FIRST_FAKE_ANSWER = "QUESTION_FIRST_FAKE_ANSWER";
    public static final String QUESTIONS_COLUMN_SECOND_FAKE_ANSWER = "QUESTION_SECOND_FAKE_ANSWER";
    public static final String QUESTIONS_COLUMN_THIRD_FAKE_ANSWER = "QUESTION_THIRD_FAKE_ANSWER";
    public static final String QUESTIONS_COLUMN_CONFIRMED = "QUESTION_CONFIRMED";

    private DbConstants() {
    }
}
