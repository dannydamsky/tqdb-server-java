package com.damsky.danny.tqdb.data.implementation;

import com.damsky.danny.tqdb.data.AppDatabase;
import com.damsky.danny.tqdb.data.DbConstants;
import com.damsky.danny.tqdb.data.dao.GameDao;
import com.damsky.danny.tqdb.data.entity.Game;

import java.util.List;

/**
 * {@link GameDao} implementation using Hibernate ORM.
 *
 * @author Danny Damsky
 * @see Game
 * @see GameDao
 */
public final class HibernateGameDao implements GameDao {
    @Override
    public void insert(final Game game) {
        appDatabase.execute(currentSession ->
                currentSession.save(game)
        );
    }

    @Override
    public void delete(final Game game) {
        appDatabase.execute(currentSession ->
                currentSession.delete(game)
        );
    }

    @Override
    public void update(final Game game) {
        appDatabase.execute(currentSession ->
                currentSession.update(game)
        );
    }

    @Override
    public List<Game> getAll() {
        //noinspection unchecked
        return appDatabase.executeAndReturn((AppDatabase.DatabaseQueryCommunicator<List<Game>>) currentSession ->
                currentSession.createSQLQuery("SELECT * FROM " + DbConstants.GAMES_TABLE_NAME + ";")
                        .addEntity(Game.class)
                        .list()
        );
    }
}
