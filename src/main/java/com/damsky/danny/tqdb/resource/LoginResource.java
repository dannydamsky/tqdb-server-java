package com.damsky.danny.tqdb.resource;

import com.damsky.danny.tqdb.data.DbHandler;
import com.damsky.danny.tqdb.data.entity.User;
import com.damsky.danny.tqdb.util.AuthUtils;
import com.damsky.danny.tqdb.util.HtmlPainter;
import com.damsky.danny.tqdb.util.json.JsonObject;
import com.damsky.danny.tqdb.util.json.JsonResult;
import com.damsky.danny.tqdb.util.json.JsonUser;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * This resource is used for logging in and registering users to the webapp's database
 * and HTTP Session (if done in HTML)
 *
 * @author Danny Damsky
 */
@Path("/login")
public final class LoginResource {

    @Context
    private HttpServletRequest httpServletRequest;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String paintLoginPage() {
        final User user = AuthUtils.isLoggedIn(httpServletRequest);
        if (user != null) {
            return HtmlPainter.paintUserPage(user);
        }
        return HtmlPainter.paintLoginPage();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String paintLoginPage(@FormParam(HtmlPainter.LOGIN_INPUT_NAME_LOGOUT) String logout) {
        if (logout != null && logout.equals(HtmlPainter.LOGIN_INPUT_NAME_LOGOUT)) {
            AuthUtils.logOut(httpServletRequest);
            return HtmlPainter.paintLoginPage();
        }
        return paintLoginPage();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject<User> registerUser(JsonUser user) {
        final User realUser = user.validateAndConvert();
        if (realUser != null) {
            DbHandler.getInstance().getUserDao().insert(realUser);
            return new JsonObject.Builder<User>().setJsonResult(JsonResult.success())
                    .setObject(realUser).build();
        }
        return new JsonObject.Builder<User>().setJsonResult(JsonResult.error("Incorrect properties; user wasn't registered."))
                .build();
    }
}
