package com.damsky.danny.tqdb.util.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class is mostly what gets returned from API calls in the servlets.
 * It's purpose is to return the caller's desired object but also include a relevant message
 * along with it in order to let the caller know a little bit more about what's going on.
 *
 * @param <T> Any type that should be returned from an API call.
 * @author Danny Damsky
 */
@XmlRootElement
public final class JsonObject<T> {

    @JsonProperty("result")
    private JsonResult jsonResult;
    @JsonProperty("object")
    private T object;

    private JsonObject(Builder<T> builder) {
        this.jsonResult = builder.jsonResult;
        this.object = builder.object;
    }

    public JsonObject() {
    }

    public JsonResult getJsonResult() {
        return jsonResult;
    }

    public void setJsonResult(JsonResult jsonResult) {
        this.jsonResult = jsonResult;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "JsonObject{" +
                "jsonResult=" + jsonResult +
                ", object=" + object +
                '}';
    }

    public static final class Builder<T> {
        private JsonResult jsonResult;
        private T object;

        public Builder<T> setJsonResult(JsonResult jsonResult) {
            this.jsonResult = jsonResult;
            return this;
        }

        public Builder<T> setObject(T object) {
            this.object = object;
            return this;
        }

        public JsonObject<T> build() {
            if (this.jsonResult == null) {
                if (this.object != null) {
                    this.jsonResult = JsonResult.success();
                } else {
                    this.jsonResult = JsonResult.error("UNKNOWN");
                }
            }
            return new JsonObject<>(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "jsonResult=" + jsonResult +
                    ", object=" + object +
                    '}';
        }
    }
}
