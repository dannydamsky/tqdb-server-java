package com.damsky.danny.tqdb.data.dao;

import com.damsky.danny.tqdb.data.AppDatabase;

import java.util.List;

/**
 * Generic interface for implementing DAO classes.
 *
 * @param <T> An entity type.
 * @author Danny Damsky
 */
public interface Dao<T> {

    AppDatabase appDatabase = AppDatabase.getInstance();

    /**
     * Inserts an entity to the database in a single transaction.
     *
     * @param object The entity to insert to the database.
     */
    void insert(final T object);

    /**
     * Deletes an entity from the database in a single transaction.
     *
     * @param object The entity to delete from the database.
     */
    void delete(final T object);

    /**
     * Updates an entity in the database in a single transaction.
     *
     * @param object The entity to update in the database.
     */
    void update(final T object);

    /**
     * Queries the database for all rows of this entity in a single transaction.
     *
     * @return All available rows of this entity in the database.
     */
    List<T> getAll();
}
