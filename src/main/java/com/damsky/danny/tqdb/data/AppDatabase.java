package com.damsky.danny.tqdb.data;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Singleton class that is used for executing database transactions and
 * holds instances of factory objects related to database work.
 *
 * @author Danny Damsky
 */
public final class AppDatabase {

//    private static EntityManagerFactory buildEntityManagerFactory(Configuration hibConfiguration) {
//
//        Properties p = hibConfiguration.getProperties();
//
//        // convert to Map
//        Map<String, String> pMap = new HashMap<>();
//        Enumeration<?> e = p.propertyNames();
//        while (e.hasMoreElements()) {
//            String s = (String) e.nextElement();
//            pMap.put(s, p.getProperty(s));
//        }
//
//        // create EntityManagerFactory
//
//        return Persistence.createEntityManagerFactory("persistence_JPA", pMap);
//    }

    private static final AppDatabase instance = new AppDatabase();

    public static AppDatabase getInstance() {
        return instance;
    }

    private final SessionFactory factory;
    //private final EntityManagerFactory entityManagerFactory;

    private AppDatabase() {
        final Configuration configuration = new Configuration().configure();
        //this.entityManagerFactory = buildEntityManagerFactory(configuration);
        this.factory = configuration.buildSessionFactory();
    }

    public void execute(final DatabaseCommunicator communicator) {
        final Session currentSession = factory.getCurrentSession();
        try {
            currentSession.beginTransaction();
            communicator.communicate(currentSession);
            currentSession.getTransaction().commit();
        } finally {
            currentSession.close();
        }
    }

    public <T> T executeAndReturn(final DatabaseQueryCommunicator<T> communicator) {
        try (final Session currentSession = factory.getCurrentSession()) {
            currentSession.beginTransaction();
            final T object = communicator.communicate(currentSession);
            currentSession.getTransaction().commit();
            return object;
        }
    }

    public interface DatabaseCommunicator {
        void communicate(Session currentSession);
    }

    public interface DatabaseQueryCommunicator<T> {
        T communicate(Session currentSession);
    }
}
