package com.damsky.danny.tqdb.data.dao;

import com.damsky.danny.tqdb.data.entity.Game;

/**
 * Interface for implementing DAO classes of type {@link Game}.
 *
 * @author Danny Damsky
 * @see Dao
 * @see Game
 */
public interface GameDao extends Dao<Game> {
}
