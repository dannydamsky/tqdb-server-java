package com.damsky.danny.tqdb.resource;

import com.damsky.danny.tqdb.data.entity.User;
import com.damsky.danny.tqdb.util.AuthUtils;
import com.damsky.danny.tqdb.util.GameUtils;
import com.damsky.danny.tqdb.util.HtmlPainter;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * This resource is used for playing the trivia game in HTML.
 *
 * @author Danny Damsky
 */
@Path("/play")
public final class PlayResource {

    @Context
    private HttpServletRequest httpServletRequest;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String paintPlayPage() {
        GameUtils.start();
        final User user = AuthUtils.isLoggedIn(httpServletRequest);
        if (user != null) {
            return HtmlPainter.paintPlayPage(user.getUsername(), GameUtils.getGameQuestions());
        }
        return HtmlPainter.paintPlayPage("Log in", GameUtils.getGameQuestions());
    }
}
