package com.damsky.danny.tqdb.resource;

import com.damsky.danny.tqdb.util.HtmlPainter;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This resource is used for displaying documentation about the webapp's API (HTML-only)
 *
 * @author Danny Damsky
 */
@Path("/docs")
public final class DocsResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String paintDocsPage() {
        return HtmlPainter.paintDocsPage();
    }
}
