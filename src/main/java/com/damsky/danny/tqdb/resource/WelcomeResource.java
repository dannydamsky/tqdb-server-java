package com.damsky.danny.tqdb.resource;

import com.damsky.danny.tqdb.data.DbHandler;
import com.damsky.danny.tqdb.data.entity.Game;
import com.damsky.danny.tqdb.data.entity.Question;
import com.damsky.danny.tqdb.data.entity.User;
import com.damsky.danny.tqdb.util.AuthUtils;
import com.damsky.danny.tqdb.util.GameUtils;
import com.damsky.danny.tqdb.util.HtmlPainter;
import com.damsky.danny.tqdb.util.json.JsonGame;
import com.damsky.danny.tqdb.util.json.JsonObject;
import com.damsky.danny.tqdb.util.json.JsonResult;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * The main resource of the application.
 *
 * @author Danny Damsky
 */
@Path("/")
public final class WelcomeResource {

    @Context
    private HttpServletRequest httpServletRequest;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String paintWelcomePage() {
        final User user = AuthUtils.isLoggedIn(httpServletRequest);
        if (user != null) {
            return HtmlPainter.paintWelcomePage(user.getUsername());
        }
        return HtmlPainter.paintWelcomePage("Log in");
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String paintWelcomePage(@FormParam(HtmlPainter.LOGIN_INPUT_NAME_EMAIL) String email,
                                   @FormParam(HtmlPainter.LOGIN_INPUT_NAME_USER) String usernameOrEmail,
                                   @FormParam(HtmlPainter.LOGIN_INPUT_NAME_PASSWORD) String password) {
        final String username;
        if (email != null) {
            username = registerUser(email, usernameOrEmail, password);
        } else {
            username = loginWithUser(usernameOrEmail, password);
        }
        return HtmlPainter.paintWelcomePage(username);
    }

    private String registerUser(String email, String username, String password) {
        final User user = new User.Builder()
                .setEmail(email)
                .setUsername(username)
                .setPassword(password)
                .build();
        AuthUtils.register(httpServletRequest, user);
        return user.getUsername();
    }

    private String loginWithUser(String usernameOrEmail, String password) {
        final User user = DbHandler.getInstance().getUserDao().getByEmailOrUsernameAndPassword(usernameOrEmail, password);
        if (user != null) {
            AuthUtils.logIn(httpServletRequest, user);
            return user.getUsername();
        }
        return "Log in";
    }

    @GET
    @Path("/questions/all/{page}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject<List<Question>> getAllQuestions(@PathParam("page") int page) {
        final List<Question> questions;
        if (page == 0) {
            questions = DbHandler.getInstance().getQuestionDao().getAllFromTo(page, GameUtils.GAME_QUESTIONS_LENGTH);
        } else {
            questions = DbHandler.getInstance().getQuestionDao().getAllFromTo((page * 10) + 1, GameUtils.GAME_QUESTIONS_LENGTH);
        }
        if (questions.size() == 0) {
            return new JsonObject.Builder<List<Question>>().setJsonResult(JsonResult.error("This page is empty")).build();
        }
        return new JsonObject.Builder<List<Question>>().setJsonResult(JsonResult.success()).setObject(questions).build();
    }

    @GET
    @Path("/questions/confirmed-only/{page}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject<List<Question>> getConfirmedQuestions(@PathParam("page") int page) {
        final List<Question> questions;
        if (page == 0) {
            questions = DbHandler.getInstance().getQuestionDao().getAllFromToConfirmed(page, GameUtils.GAME_QUESTIONS_LENGTH);
        } else {
            questions = DbHandler.getInstance().getQuestionDao().getAllFromToConfirmed((page * 10) + 1, GameUtils.GAME_QUESTIONS_LENGTH);
        }
        if (questions.size() == 0) {
            return new JsonObject.Builder<List<Question>>().setJsonResult(JsonResult.error("This page is empty")).build();
        }
        return new JsonObject.Builder<List<Question>>().setJsonResult(JsonResult.success()).setObject(questions).build();
    }

    @GET
    @Path("/questions/random/{count}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject<List<Question>> getGeneratedQuestions(@PathParam("count") int count) {
        if (count <= 25 && count > 0) {
            return new JsonObject.Builder<List<Question>>()
                    .setJsonResult(JsonResult.success())
                    .setObject(DbHandler.getInstance().getQuestionDao().getRandom(count)).build();
        } else {
            return new JsonObject.Builder<List<Question>>()
                    .setJsonResult(JsonResult.error("Amount of results must be between 1 and 25"))
                    .build();
        }
    }

    @GET
    @Path("/questions/random-confirmed-only/{count}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject<List<Question>> getGeneratedConfirmedQuestions(@PathParam("count") int count) {
        if (count <= 25 && count > 0) {
            return new JsonObject.Builder<List<Question>>()
                    .setJsonResult(JsonResult.success())
                    .setObject(DbHandler.getInstance().getQuestionDao().getRandomConfirmed(count)).build();
        } else {
            return new JsonObject.Builder<List<Question>>()
                    .setJsonResult(JsonResult.error("Amount of results must be between 1 and 25"))
                    .build();
        }
    }

    @GET
    @Path("/games")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject<List<JsonGame>> getUserGames(@QueryParam("key") String apiKey) {
        final User user = DbHandler.getInstance().getUserDao().getByApiKey(apiKey);
        final JsonObject<List<JsonGame>> object = new JsonObject<>();
        if (user != null) {
            final List<Game> games = user.getGames();
            final int size = games.size();
            final List<JsonGame> jsonGames = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                final Game game = games.get(i);
                jsonGames.add(i, new JsonGame(game.getScore(), game.getWhen()));
            }
            object.setJsonResult(JsonResult.success());
            object.setObject(jsonGames);
        } else {
            object.setJsonResult(JsonResult.error("Invalid API key"));
        }
        return object;
    }
}
