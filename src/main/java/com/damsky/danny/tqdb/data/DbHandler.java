package com.damsky.danny.tqdb.data;

import com.damsky.danny.tqdb.data.dao.GameDao;
import com.damsky.danny.tqdb.data.dao.QuestionDao;
import com.damsky.danny.tqdb.data.dao.UserDao;
import com.damsky.danny.tqdb.data.implementation.HibernateGameDao;
import com.damsky.danny.tqdb.data.implementation.HibernateQuestionDao;
import com.damsky.danny.tqdb.data.implementation.HibernateUserDao;

/**
 * This is a singleton class that contains instances of EntityDAO
 * implementations.
 *
 * @author Danny Damsky
 */
public final class DbHandler {
    private static final DbHandler instance = new DbHandler();

    public static DbHandler getInstance() {
        return instance;
    }

    private final UserDao userDao;
    private final GameDao gameDao;
    private final QuestionDao questionDao;

    private DbHandler() {
        this.userDao = new HibernateUserDao();
        this.gameDao = new HibernateGameDao();
        this.questionDao = new HibernateQuestionDao();
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public GameDao getGameDao() {
        return gameDao;
    }

    public QuestionDao getQuestionDao() {
        return questionDao;
    }
}
