package com.damsky.danny.tqdb.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Utility class for formatting date.
 *
 * @author Danny Damsky
 */
final class DateUtils {

    private DateUtils() {
    }

    /**
     * @param timeInMillis Time in milliseconds
     * @return time string with the following formatting: <Br/>DD/MM/YY HH:MM
     */
    static String format(long timeInMillis) {
        final Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(timeInMillis);

        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        final int month = calendar.get(Calendar.MONTH);
        final int year = calendar.get(Calendar.YEAR);
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);

        return String.valueOf(day < 10 ? "0" + day : day) + "/" +
                (month < 10 ? "0" + month : month) + "/" +
                year + " " +
                (hour < 10 ? "0" + hour : hour) + ":" +
                (minute < 10 ? "0" + minute : minute);
    }
}
