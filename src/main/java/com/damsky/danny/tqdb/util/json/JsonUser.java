package com.damsky.danny.tqdb.util.json;

import com.damsky.danny.tqdb.data.entity.User;
import com.damsky.danny.tqdb.util.AuthUtils;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A lightweight version of the {@link User} class.
 * It's used for getting and passing JSONs using the API.
 *
 * @author Danny Damsky
 */
@XmlRootElement
public final class JsonUser {

    @JsonProperty("email")
    private String email;
    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;

    // Reserved for Jackson JSON Parser
    public JsonUser() {
    }

    /**
     * @return Makes sure that the object's fields are valid and if they are
     * then it returns a {@link User} object with the same fields, if not, returns null.
     */
    public User validateAndConvert() {
        if (validateStrings(email, username, password)) {
            if (AuthUtils.isValidEmail(email) &&
                    AuthUtils.isValidUsername(username) &&
                    AuthUtils.isValidPassword(password)) {
                return new User.Builder()
                        .setEmail(email)
                        .setUsername(username)
                        .setPassword(password)
                        .build();
            }
        }
        return null;
    }

    private boolean validateStrings(final String... strings) {
        for (final String string : strings) {
            if (string == null || string.trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "JsonUser{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
