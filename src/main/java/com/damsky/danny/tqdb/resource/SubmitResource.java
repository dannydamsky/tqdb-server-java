package com.damsky.danny.tqdb.resource;

import com.damsky.danny.tqdb.data.DbHandler;
import com.damsky.danny.tqdb.data.entity.Game;
import com.damsky.danny.tqdb.data.entity.Question;
import com.damsky.danny.tqdb.data.entity.User;
import com.damsky.danny.tqdb.util.AuthUtils;
import com.damsky.danny.tqdb.util.GameUtils;
import com.damsky.danny.tqdb.util.HtmlPainter;
import com.damsky.danny.tqdb.util.json.JsonGame;
import com.damsky.danny.tqdb.util.json.JsonQuestion;
import com.damsky.danny.tqdb.util.json.JsonResult;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * This resource is used for submitting data, such as games and questions.
 *
 * @author Danny Damsky
 */
@Path("/submit")
public final class SubmitResource {

    @Context
    private HttpServletRequest httpServletRequest;

    @POST
    @Path("/question")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String paintSuccessPage(@FormParam(HtmlPainter.QUESTION_INPUT_NAME_QUESTION) String question,
                                   @FormParam(HtmlPainter.QUESTION_INPUT_NAME_REAL_ANSWER) String realAnswer,
                                   @FormParam(HtmlPainter.QUESTION_INPUT_NAME_FIRST_FAKE_ANSWER) String firstFakeAnswer,
                                   @FormParam(HtmlPainter.QUESTION_INPUT_NAME_SECOND_FAKE_ANSWER) String secondFakeAnswer,
                                   @FormParam(HtmlPainter.QUESTION_INPUT_NAME_THIRD_FAKE_ANSWER) String thirdFakeAnswer) {
        DbHandler.getInstance().getQuestionDao().insert(new Question.Builder()
                .setQuestion(question).setRealAnswer(realAnswer).setFirstFakeAnswer(firstFakeAnswer)
                .setSecondFakeAnswer(secondFakeAnswer).setThirdFakeAnswer(thirdFakeAnswer).build());
        return HtmlPainter.paintSubmitQuestionPage();
    }

    @POST
    @Path("/question")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public JsonResult addQuestionAndReturnResult(JsonQuestion jsonQuestion) {
        final Question question = jsonQuestion.validateAndConvert();
        if (question != null) {
            DbHandler.getInstance().getQuestionDao().insert(question);
            return JsonResult.success();
        } else {
            return JsonResult.error("Your question JSON had missing or incorrect data.");
        }
    }

    @POST
    @Path("/game")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String paintSuccessPage(@FormParam(HtmlPainter.GAME_INPUT_NAME_ONE) String one,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_TWO) String two,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_THREE) String three,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_FOUR) String four,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_FIVE) String five,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_SIX) String six,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_SEVEN) String seven,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_EIGHT) String eight,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_NINE) String nine,
                                   @FormParam(HtmlPainter.GAME_INPUT_NAME_TEN) String ten) {
        final int score = GameUtils.calculateResults(one, two, three, four, five, six, seven, eight, nine, ten);
        final String apiKey = AuthUtils.getApiKey(httpServletRequest);
        if (apiKey != null) {
            final Game game = new Game.Builder().setScore(score).setWhen(System.currentTimeMillis()).build();
            DbHandler.getInstance().getUserDao().addGameToUserByApiKey(apiKey, game);
        }
        return HtmlPainter.paintSubmitGamePage(score);
    }

    @POST
    @Path("/game")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public JsonResult addGameToUser(@QueryParam("key") String apiKey, JsonGame game) {
        final User user = DbHandler.getInstance().getUserDao().getByApiKey(apiKey);
        if (user != null) {
            final Game realGame = game.validateAndConvert();
            if (realGame != null) {
                realGame.setUser(user);
                user.getGames().add(realGame);
                DbHandler.getInstance().getUserDao().update(user);
                return JsonResult.success();
            } else {
                return JsonResult.error("Invalid game properties");
            }
        }
        return JsonResult.error("Invalid API key");
    }
}
