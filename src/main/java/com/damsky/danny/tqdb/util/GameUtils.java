package com.damsky.danny.tqdb.util;

import com.damsky.danny.tqdb.data.DbHandler;
import com.damsky.danny.tqdb.data.entity.Question;

import java.util.List;

/**
 * Utility class for handling game events in HTML.
 *
 * @author Danny Damsky
 */
public final class GameUtils {
    private static List<Question> GAME_QUESTIONS;
    public static final int GAME_QUESTIONS_LENGTH = 10;

    public static List<Question> getGameQuestions() {
        return GAME_QUESTIONS;
    }

    /**
     * Initializes itself with 10 random questions from the database.
     */
    public static void start() {
        GAME_QUESTIONS = DbHandler.getInstance().getQuestionDao().getRandomConfirmed(GAME_QUESTIONS_LENGTH);
    }

    /**
     * @param results The user's answers.
     * @return int representing a value out of 10 according to how many
     * answers the user answered correctly.
     */
    public static int calculateResults(final String... results) {
        int score = 0;
        for (int i = 0; i < GAME_QUESTIONS_LENGTH; i++) {
            if (GAME_QUESTIONS.get(i).getRealAnswer().equals(results[i])) {
                score++;
            }
        }
        GAME_QUESTIONS = null;
        return score;
    }
}
