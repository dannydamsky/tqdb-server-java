package com.damsky.danny.tqdb;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

/**
 * Java Jersey application. Holds references to Java Jersey resource files.
 *
 * @author Danny Damsky
 */
@ApplicationPath("/web")
public final class QuestionnaireApplication extends ResourceConfig {

    public QuestionnaireApplication() {
        packages("com.damsky.danny.tqdb.resource");
    }
}
