package com.damsky.danny.tqdb.util;

import com.damsky.danny.tqdb.data.DbConstants;
import com.damsky.danny.tqdb.data.DbHandler;
import com.damsky.danny.tqdb.data.entity.User;
import org.apache.commons.validator.routines.EmailValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.Random;

/**
 * Utility class for handling authentication.
 *
 * @author Danny Damsky;
 */
public final class AuthUtils {

    private static final int KEY_LENGTH = 32;

    private static final char[] ALL_CHARS = {
            // a-z
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            // A-Z
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            // 0-9
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };

    private static final int ALL_CHARS_LENGTH = 62;

    static final String REGEX_USERNAME_VALIDATION = "^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$";
    static final String REGEX_PASSWORD_VALIDATION = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=_])(?=\\S+$).{8,48}$";

    private AuthUtils() {
    }

    /**
     * @return Generates a random key for use with the webapp's API.
     */
    public static String generateKey() {
        final StringBuilder password = new StringBuilder(KEY_LENGTH);
        final Random random = RandomUtils.getRandomAggressive();
        for (int i = 0; i < KEY_LENGTH; i++) {
            password.append(ALL_CHARS[random.nextInt(ALL_CHARS_LENGTH)]);
        }
        return password.toString();
    }

    /**
     * In order for this function to return true, the username must not contain any spaces
     * and should be between 2 and 20 characters long.
     *
     * @param username The username to validate.
     * @return True if the username is valid according to the class' specifications.
     */
    public static boolean isValidUsername(final String username) {
        return username.matches(REGEX_USERNAME_VALIDATION);
    }

    /**
     * In order for this function to return true, the password must be between 8 and 48 characters long,
     * and should contain at least one uppercase letter, one lowercase letter and one special character.
     *
     * @param password The password to validate.
     * @return True if the password is valid according to the class' specifications.
     */
    public static boolean isValidPassword(final String password) {
        return password.matches(REGEX_PASSWORD_VALIDATION);
    }

    /**
     * @param usernameOrEmail A user's username or e-mail address.
     * @return True if it's an e-mail address and false if it's a username.
     */
    public static boolean isValidEmail(final String usernameOrEmail) {
        return EmailValidator.getInstance().isValid(usernameOrEmail);
    }

    /**
     * @param request The servlet's request object.
     * @return The logged-in user's object, or null if the user isn't logged in.
     */
    public static User isLoggedIn(final HttpServletRequest request) {
        final String apiKey = getApiKey(request);
        if (apiKey != null) {
            return DbHandler.getInstance().getUserDao().getByApiKey(apiKey);
        }
        return null;
    }

    /**
     * @param request The servlet's request object.
     * @return The API key that's registered in the request's HTTP session, or null
     * if the user is not logged in.
     */
    public static String getApiKey(final HttpServletRequest request) {
        return (String) request.getSession().getAttribute(DbConstants.USERS_COLUMN_API_KEY);
    }

    /**
     * Adds the user's API key to the current HTTP Session.
     *
     * @param request The servlet's request object.
     * @param user    The user to store in the HTTP Session.
     */
    public static void logIn(final HttpServletRequest request, final User user) {
        request.getSession().setAttribute(DbConstants.USERS_COLUMN_API_KEY, user.getApiKey());
    }

    /**
     * Removes the user's API key from the current HTTP Session.
     * @param request The servlet's request object
     */
    public static void logOut(final HttpServletRequest request) {
        request.getSession().removeAttribute(DbConstants.USERS_COLUMN_API_KEY);
    }

    /**
     * Inserts the new user to the database and stores him in the HTTP Session.
     * @param request The servlet's request object.
     * @param user The user to add to the database and store in the HTTP Session.
     */
    public static void register(final HttpServletRequest request, final User user) {
        DbHandler.getInstance().getUserDao().insert(user);
        logIn(request, user);
    }
}
